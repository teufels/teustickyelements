# 2.2.1
* edit buybox only use override if sticky cart is enabled

# 2.2.0
* edit configurator select to new default (option name) > 6.6.4

# 2.0.1
* fix fix-to default

# 1.1.6
* fix fix-to default

# 1.1.5
* configuration to force display options as text

# 1.1.4
* fix mobile Variant switch

# 1.1.3
* fix select for sticky cart

# 1.1.2
* add viewport 'none' for oder sw versions

# 1.1.1
* add debug for sticky cart

# 1.1.0
* add sticky cart

# 1.0.2
* edit js new eventlistner (Viewport/hasChanged)
* edit static selector for cms-block

# 1.0.1
* fix js
  * remove doubled classList

# 1.0.0
* first release