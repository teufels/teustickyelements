<?php declare(strict_types=1);

namespace TeuStickyElements;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;
use Shopware\Core\Framework\Plugin\Util\PluginIdProvider;
use Shopware\Core\System\CustomField\CustomFieldTypes;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Routing\RouteCollectionBuilder;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelDefinition;

class TeuStickyElements extends Plugin
{
    public function install(InstallContext $context): void {
        $this->checkCustomFields($context);
    }

    public function update(UpdateContext $context): void
    {
        $this->checkCustomFields($context);
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        //$this->container = $container;
    }

    public function getViewPaths(): array
    {
        return [
            '/Resources/views'
        ];
    }

    private function checkCustomFields($context)
    {
        $customCatFields = [

            [
                'id' => Uuid::fromBytesToHex('teuStickyCartPro'),
                'name' => 'teu_product_sticky_cart',
                'active' => false,
                'position' => 10,
                'config' => [
                    'label' => [
                        'en-GB' => 'Sticky Cart',
                        'de-DE' => 'Sticky Cart'
                    ],
                ],
                'customFields' => [
                    [
                        'id' => Uuid::fromBytesToHex('stickyCartDeacti'),
                        'name' => 'deactivateStickyCart',
                        'type' => CustomFieldTypes::BOOL,
                        'config' => [
                            'customFieldPosition' => 1,
                            'label' => [
                                'en-GB' => 'Deactivate sticky cart',
                                'de-DE' => 'Sticky Cart deaktivieren',
                            ],
                            'componentName' => 'sw-field',
                            'customFieldType' => 'switch',
                        ]
                    ],
                    [
                        'id' => Uuid::fromBytesToHex('stickyCartDeConf'),
                        'name' => 'deactivateStickyCartConf',
                        'type' => CustomFieldTypes::BOOL,
                        'config' => [
                            'customFieldPosition' => 1,
                            'label' => [
                                'en-GB' => 'Deactivate variant configuration on sticky cart',
                                'de-DE' => 'Varianten Konfiguration im Sticky Warenkorb deaktivieren',
                            ],
                            'componentName' => 'sw-field',
                            'customFieldType' => 'switch',
                        ]
                    ]
                ],
                'relations' => [
                    [
                        'id' => Uuid::fromBytesToHex('teuProductOrdAdd'),
                        'entityName' => 'product'
                    ]
                ]
            ],
        ];

        /** @var EntityRepositoryInterface $repo */
        $repo = $this->container->get('custom_field_set.repository');

        foreach ($customCatFields as $customCatFieldSet) {
            $repo->upsert([$customCatFieldSet], $context->getContext());
        }
    }
}
