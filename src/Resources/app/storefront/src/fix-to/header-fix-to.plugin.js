import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';

export default class HeaderFixToPlugin extends Plugin {

    static options = {
        staticHeaderFixToSelector: 'body',
        hideOnScrollDown: false,
        topbarSelector: '.top-bar-nav',
        hideClass: 'hide',
        scrollTopOffset: 100,
        zIndex: 9999,
        fixedClass: 'fixto-fixed',
        useNativeSticky: false,
        mind: false,
        mindBottomPadding: true,
        mindViewport: true
    };
    init() {
        const { hideOnScrollDown } = this.options;

        this.initStickyHeader();
        if (hideOnScrollDown === true) {
           this.initHideOnScrollDown();
        }
    }

    initStickyHeader() {
        const { zIndex, fixedClass, useNativeSticky, mind, mindBottomPadding, mindViewport, topbarSelector, staticHeaderFixToSelector } = this.options;

        this.stickyHeaderFixToElement = DomAccess.querySelector(document, staticHeaderFixToSelector);
        this.stickyHeaderMarginTopElement = DomAccess.querySelector(this.el, topbarSelector);

        let stickyHeader = fixto.fixTo(this.el, this.stickyHeaderFixToElement, {
            className: fixedClass,
            useNativeSticky: useNativeSticky,
            top: this.stickyHeaderMarginTopElement.offsetHeight > 0 ? -this.stickyHeaderMarginTopElement.offsetHeight : -1,
            zIndex: zIndex,
            mind: mind,
            mindBottomPadding: mindBottomPadding,
            mindViewport: mindViewport
        });

        let that = this;
        document.addEventListener('Viewport/hasChanged', () => {
            stickyHeader.setOptions({
                top: that.stickyHeaderMarginTopElement.offsetHeight > 0 ? -that.stickyHeaderMarginTopElement.offsetHeight : -1,
            });
        });

        /*window.addEventListener("resize", function () {
            stickyHeader.setOptions({
                top: that.stickyHeaderMarginTopElement.offsetHeight > 0 ? -that.stickyHeaderMarginTopElement.offsetHeight : -1,
            });
        });*/
    }

    initHideOnScrollDown() {
        let that = this;
        let lastScrollTop = 0;
        window.addEventListener("scroll", function () {
            const { scrollTopOffset, hideClass } = that.options;

            if (document.documentElement.scrollTop > scrollTopOffset) {
                let st = window.pageYOffset || document.documentElement.scrollTop;

                if (st > lastScrollTop) {
                    if (!that.el.classList.contains('hide')) {
                        that.el.classList.add(hideClass);
                    }
                } else if (st < lastScrollTop) {
                    if (that.el.classList.contains('hide')) {
                        that.el.classList.remove(hideClass);
                    }
                }

                lastScrollTop = st <= 0 ? 0 : st;
            }
        });
    }

}