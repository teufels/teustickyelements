import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';
import ViewportDetection from 'src/helper/viewport-detection.helper';


export default class GalleryPluginFixTo extends Plugin {

    static options = {
        stickyGalleryFixToElementSelector: '.product-detail-main',
        zIndex: 1,
        fixedClass: 'fixto-fixed',
        useNativeSticky: false,
        mind: 'null',
        mindBottomPadding: true,
        mindViewport: true,
        top:0
    };
    init() {
        this.initStickyGallery();
    }

    initStickyGallery() {
        const { stickyGalleryFixToElementSelector ,zIndex, fixedClass, useNativeSticky, mind, mindBottomPadding, mindViewport,top, staticHeaderFixToSelector } = this.options;

        let stickyGalleryFixToElement = DomAccess.querySelector(document, stickyGalleryFixToElementSelector);


        let stickyGalleryFix = fixto.fixTo(this.el, stickyGalleryFixToElement, {
            className: fixedClass,
            useNativeSticky: useNativeSticky,
            top: top,
            zIndex: zIndex,
            mind: mind,
            mindBottomPadding: mindBottomPadding,
            mindViewport: mindViewport
        });

        if (ViewportDetection.isXS() || ViewportDetection.isSM() || ViewportDetection.isMD()) {
            stickyGalleryFix.stop()
        }

        document.addEventListener('Viewport/hasChanged', () => {
            if (ViewportDetection.isXS() || ViewportDetection.isSM() || ViewportDetection.isMD()) {
                stickyGalleryFix.stop()
            } else {
                stickyGalleryFix.start()
            }
        });


        /*
        let header = DomAccess.querySelector(document, '[ data-header-fix-to]');

        function classChanged(b) {
            if (b.classList.contains('hide')) {
                console.log('hidden');
                stickyGalleryFix.setOptions({
                    mind: 'null'
                });
            } else {
                console.log('show');
                stickyGalleryFix.setOptions({
                    mind: mind
                });
            }
            stickyGalleryFix.refresh();
        }

        var ob = new MutationObserver(function() {
            classChanged(header);
        });

        ob.observe(header, {
            attributes: true,
            attributeFilter: ["class"]
        });

        window.addEventListener("scroll", function () {
            stickyGalleryFix.refresh();
        });

        */
    }

}