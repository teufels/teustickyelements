import Plugin from 'src/plugin-system/plugin.class';
import DomAccess from 'src/helper/dom-access.helper';

export default class FixToPlugin extends Plugin {

    static options = {
        staticHeaderFixElementSelector: null,
        staticHeaderFixToSelector: null,
        zIndex: 1,
        fixedClass: 'fixto-fixed',
        useNativeSticky: false,
        mind: false,
        mindBottomPadding: true,
        mindViewport: true,
        top: 0
    };
    init() {

        this.initFixedElement();

    }

    initFixedElement() {
        const { staticHeaderFixElementSelector, staticHeaderFixToSelector, zIndex, fixedClass, useNativeSticky, mind, mindBottomPadding, mindViewport, top } = this.options;

        let staticHeaderFixElement = this.el;
        if (staticHeaderFixElementSelector) {
            staticHeaderFixElement = DomAccess.querySelector(document, staticHeaderFixElementSelector);
        }

        let staticHeaderFixToElement = this.el.parentNode.el;
        if (staticHeaderFixToSelector) {
            staticHeaderFixToElement = DomAccess.querySelector(document, staticHeaderFixToSelector);
        }

        this.initFixTo(staticHeaderFixElement, staticHeaderFixToElement, zIndex, fixedClass, useNativeSticky, mind, mindBottomPadding, mindViewport, top);
    }

    initFixTo(fixToElement, fixToFix , zIndex, fixedClass, useNativeSticky, mind, mindBottomPadding, mindViewport, top) {
        let fixTo = fixto.fixTo(fixToElement, fixToFix, {
            className: fixedClass,
            useNativeSticky: useNativeSticky,
            top: top,
            zIndex: zIndex,
            mind: mind,
            mindBottomPadding: mindBottomPadding,
            mindViewport: mindViewport
        });
    }


}