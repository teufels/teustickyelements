import FixToPlugin from './fix-to/fix-to.plugin'
import HeaderFixToPlugin from './fix-to/header-fix-to.plugin'
import GalleryFixToPlugin from "./fix-to/gallery-fix-to.plugin";
import StickyCart from './sticky-cart/sticky-cart.plugin';

const PluginManager = window.PluginManager;

PluginManager.register('FixToPlugin', FixToPlugin, '[data-fix-to]');
PluginManager.register('HeaderFixToPlugin', HeaderFixToPlugin, '[data-header-fix-to]');
PluginManager.register('GalleryFixToPlugin', GalleryFixToPlugin, '[data-gallery-fix-to]');

PluginManager.register('StickyCart', StickyCart, '[data-sticky-cart]');


