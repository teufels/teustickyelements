import Plugin from 'src/plugin-system/plugin.class';
import ViewportDetection from 'src/helper/viewport-detection.helper';

export default class stickyCart extends Plugin {

    static options = {
        quantitySelectionSelector: '.buy-widget-container select[class*="product-detail-quantity"], .buy-widget-container input[class*="product-detail-quantity"]',
        offsetTop: 250,
        offsetBottom: 100,
        scrollDebounceTime: 35,
        hiddenClass: "js-hidden",
        viewports: "['XS', 'SM', 'MD', 'LG', 'XL', 'XXL']",
        modalSelector: ".modal",
        pseudoModalTriggerSelector: '[data-bs-toggle="modal"][data-url]',
        scrollUpButtonHidden: !1,
        scrollUpButtonSelector: ".js-scroll-up-button",
        scrollUpButtonVisibleCls: "is-visible",
        debug: false
    };

    init() {
        const configurator = this.el.querySelector('.product-detail-configurator-groups-toggle-container');
        if (this.options.debug) {
            console.log("Options:" + this.options);
            console.log(this.options);
        }
        if (configurator != null) {

            this.checkConfiguratorWidth = this.checkConfiguratorWidth.bind(this);
            this.checkConfiguratorWidth();
            window.addEventListener('resize', this.checkConfiguratorWidth);

        }

        this.displayManager = this.displayManager.bind(this);
        window.addEventListener('scroll', this.displayManager);
        window.addEventListener('resize', this.displayManager);

        const bodyHeight = document.querySelector('body').scrollHeight;
        this._bottomFadeOutThreshold = bodyHeight - this.options.offsetBottom;
        this.displayManager();
    }

    // Trigger if window is scrolled to top or bottom
    displayManager() {
        if (this.options.debug) {
            console.log("Current Viewport: " + ViewportDetection.getCurrentViewport());
        }
        if(!this.options.viewports.includes(ViewportDetection.getCurrentViewport())){
            if (this.options.debug) {
                console.log("Hide of viewport");
            }
            this.el.classList.add(this.options.hiddenClass);
            return;
        }

        const bottomEdge = window.scrollY + window.innerHeight;

        if (this.options.debug) {
            console.log("_bottomFadeOutThreshold: " + this._bottomFadeOutThreshold);
            console.log("bottomEdge: " + bottomEdge);
            console.log("window.scrollY: " + window.scrollY);
        }

        if (window.scrollY > this.options.offsetTop && bottomEdge < this._bottomFadeOutThreshold) {
            if (this.options.debug) {
                console.log("show");
            }
            this.el.classList.remove(this.options.hiddenClass)
        } else {
            if (this.options.debug) {
                console.log("Hide offset top or bottom");
            }
            this.el.classList.add(this.options.hiddenClass)
        }

    }


    // Close container if window is scrolled to top or bottom
    // Triggered by displayManager()
    closeStickyContainer(pos) {
        const container = this.el;
        const containerHeight = container.offsetHeight;
        if (this.options.debug) {
            console.log("close container");
        }
        container.classList.add(this.options.hiddenClass);

    }

    // Open container if window is scrolled out of top or bottom
    // Triggered by displayManager()
    openStickyContainer(pos) {
        const container = this.el;
        if (this.options.debug) {
            console.log("open container");
        }
        container.classList.remove(this.options.hiddenClass);
    }


    checkConfiguratorWidth() {
        const container = this.el.querySelector('.product-detail-configurator-groups-toggle-container');
        const group = this.el.querySelector('.product-detail-configurator-groups-toggle-container');
        const toggleBtn = this.el.querySelector('#product-detail-configurator-grouped-toggle');
        const groupChildren = group.children;
        var groupChildrenWidth = 0;
        const _this = this;

        for (var i = 0; i < groupChildren.length; i++) {
            groupChildrenWidth += groupChildren[i].offsetWidth + 16;
        }

        groupChildrenWidth -= (toggleBtn.offsetWidth + 16);

        if (groupChildrenWidth - 16 > container.offsetWidth) {
            this.toggleToggleButton("show");
        }
        if (groupChildrenWidth - 16 <= container.offsetWidth) {
            this.toggleToggleButton("hide");
        }
    }

    toggleToggleButton(attr) {
        const toggleBtn = this.el.querySelector('#product-detail-configurator-grouped-toggle');
        const toggleBtnLable = this.el.querySelector('#product-detail-configurator-grouped-toggle .product-detail-configurator-groups-toggle-label');
        const groupsBtn = this.el.querySelectorAll('.product-detail-configurator-groups-single-toggle');
        const container = this.el.querySelector('.product-detail-configurator-groups-toggle-container');
        const viewportWidth = window.innerWidth;
        if (viewportWidth < 576 ) {
            groupsBtn.forEach( el => {
                el.classList.add(this.options.hiddenClass);
            })
            toggleBtn.classList.remove(this.options.hiddenClass);
            if (toggleBtn.offsetWidth > container.offsetWidth) {
                toggleBtnLable.classList.add(this.options.hiddenClass);
            }
        } else {
            if (attr === "show") {
                groupsBtn.forEach( el => {
                    el.classList.add(this.options.hiddenClass);
                })
                toggleBtn.classList.remove(this.options.hiddenClass);
                if (toggleBtn.offsetWidth > container.offsetWidth) {
                    toggleBtnLable.classList.add(this.options.hiddenClass);
                }
            }
            if (attr === "hide") {
                groupsBtn.forEach( el => {
                    el.classList.remove(this.options.hiddenClass);
                })
                toggleBtn.classList.add(this.options.hiddenClass);
            }
        }
    }

}